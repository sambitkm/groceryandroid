package com.app.grocery;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by EOHPA on 19.07.2015.
 */
public abstract class BaseActivity extends ActionBarActivity {
    public abstract boolean hideNotificationBar();

    public abstract int getScreenLayoutId();

    public abstract void intializeUI();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (hideNotificationBar())
        {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(getScreenLayoutId());
        intializeUI();

    }
}

