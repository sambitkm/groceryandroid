package com.app.grocery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

import grocery.app.test.com.grocery.R;

public class SplashActivity extends Activity {

    private static final int SPLASH_TIMEOUT = 2000;
    private static boolean isDestroyed;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    private void launchNewScreen(){
        isDestroyed = false;
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if (!isDestroyed)
                {
                    // Move to login page else ignore as app is closed by the
                    // user
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, SPLASH_TIMEOUT);
    }
    @Override
    protected void onResume() {
        super.onResume();
        launchNewScreen();
    }


}
